/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eitccorp;

import com.alibaba.fastjson.JSON;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Brian Sherman
 */
public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    File file;

    long timestamp;
    int satelliteID;
    double redHighLimit;
    double yellowHighLimit;
    double yellowLowLimit;
    double redLowLimit;
    double rawValue;
    Component component;

    public enum Component {
        TSTAT,
        BATT
    }

    public class Telemetry {

        long timestamp;
        double redHighLimit;
        double yellowHighLimit;
        double yellowLowLimit;
        double redLowLimit;
        double rawValue;
    }

    public class Alert {

        /**
         * @return the satelliteId
         */
        public int getSatelliteId() {
            return satelliteId;
        }

        /**
         * @param satelliteId the satelliteId to set
         */
        public void setSatelliteId(int satelliteId) {
            this.satelliteId = satelliteId;
        }

        /**
         * @return the severity
         */
        public String getSeverity() {
            return severity;
        }

        /**
         * @param severity the severity to set
         */
        public void setSeverity(String severity) {
            this.severity = severity;
        }

        /**
         * @return the component
         */
        public String getComponent() {
            return component;
        }

        /**
         * @param component the component to set
         */
        public void setComponent(String component) {
            this.component = component;
        }

        /**
         * @return the timeStamp
         */
        public String getTimestamp() {
            return timestamp;
        }

        /**
         * @param timeStamp the timeStamp to set
         */
        public void setTimestamp(String timeStamp) {
            this.timestamp = timeStamp;
        }

        private int satelliteId;
        private String severity;
        private String component;
        private String timestamp;
    }

    Map<Integer, Map<Component, List<Telemetry>>> satelliteTelemetry = new HashMap<>();

    public Main() {
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        new Main().startup();
        
    }
    
    public void startup()
    {
        File telemetryFile = new File("telemetry.txt");
        readTelemetryFile(telemetryFile);

        checkForBatteryVoltageAlert();

        checkForThermostatHighAlert();
    }

    public void readTelemetryFile(File telemetryFile) {
        Scanner scanner;
        try {
            scanner = new Scanner(telemetryFile);

            while (scanner.hasNextLine()) {
                String line;
                line = scanner.nextLine();
                byte[] bytes = line.getBytes();
                processTelemetryRow(bytes);
            }
        } catch (FileNotFoundException ex) {
            LOGGER.error("File not found exception");
        }
    }

    public void processTelemetryRow(byte[] bytes) {

        String record = new String(bytes);
        String[] recordSplit = record.split("\\|", 9);

        try {
            timestamp = convertTelemetryDateToJavaTimestamp(recordSplit[0]);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        satelliteID = Integer.parseInt(recordSplit[1]);
        redHighLimit = Double.parseDouble(recordSplit[2]);
        yellowHighLimit = Double.parseDouble(recordSplit[3]);
        yellowLowLimit = Double.parseDouble(recordSplit[4]);
        redLowLimit = Double.parseDouble(recordSplit[5]);
        rawValue = Double.parseDouble(recordSplit[6]);
        component = Component.valueOf(recordSplit[7]);

        Telemetry telemetry = new Telemetry();
        telemetry.timestamp = timestamp;
        telemetry.redHighLimit = redHighLimit;
        telemetry.yellowHighLimit = yellowHighLimit;
        telemetry.yellowLowLimit = yellowLowLimit;
        telemetry.redLowLimit = redLowLimit;
        telemetry.rawValue = rawValue;

        //Place telemetry data into satellite telemetry data model
        if (satelliteTelemetry.containsKey(satelliteID)) {
            if (satelliteTelemetry.get(satelliteID).containsKey(component)) {
                satelliteTelemetry.get(satelliteID).get(component).add(telemetry);
            } else {
                satelliteTelemetry.get(satelliteID).put(component, new ArrayList<>());
                satelliteTelemetry.get(satelliteID).get(component).add(telemetry);
            }
        } else {
            satelliteTelemetry.put(satelliteID, new HashMap<>());
            satelliteTelemetry.get(satelliteID).put(component, new ArrayList<>());
            satelliteTelemetry.get(satelliteID).get(component).add(telemetry);
        }
    }

    public long convertTelemetryDateToJavaTimestamp(String dateString) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd kk:mm:ss.SSS");
        Date date = dateFormat.parse(dateString);
        return date.getTime();
    }

    public String convertJavaTimestampToAlertDate(long timestamp) {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME.withZone(ZoneId.of("America/New_York"));
        return formatter.format(Instant.ofEpochMilli(timestamp));
    }

    /*
     * If for the same satellite there are three battery voltage readings that are under the red low limit within a five minute interval.
     * @return the number of alerts found
     */
    public int checkForBatteryVoltageAlert() {

        int batteryLowCount = 0;
        int alertCount = 0;

        //for each satellite in data
        for (Map.Entry<Integer, Map<Component, List<Telemetry>>> satellite : satelliteTelemetry.entrySet()) {

            //for each component (battery / thermostat)
            for (Map.Entry<Component, List<Telemetry>> component : satellite.getValue().entrySet()) {

                if (component.getKey() == Component.BATT) {

                    //loop through telemetry for Battery component, startindex slides window start
                    List<Telemetry> batteryList = component.getValue();
                    for (int startIndex = 0; startIndex < batteryList.size(); startIndex++) {

                        //reset battery low count
                        batteryLowCount = 0;

                        //walk up telemetry list for 5 minute window
                        for (int walkingIndex = startIndex; walkingIndex < batteryList.size(); walkingIndex++) {

                            //check if timestamp is within 5 min window from start of window
                            if (batteryList.get(walkingIndex).timestamp < batteryList.get(startIndex).timestamp + 300000) {

                                //check telemetry against alert logic - battery voltage is less than red low limit
                                if (batteryList.get(walkingIndex).rawValue < batteryList.get(walkingIndex).redLowLimit) {

                                    //increment battery low count
                                    batteryLowCount++;

                                    if (batteryLowCount >= 3) {

                                        alertCount++;

                                        //Create Alert
                                        Alert alert = new Alert();
                                        alert.setSatelliteId((int) satellite.getKey());
                                        alert.setSeverity("RED LOW");
                                        alert.setComponent(component.getKey().toString());
                                        alert.setTimestamp(convertJavaTimestampToAlertDate(batteryList.get(startIndex).timestamp));

                                        //pretty print as json to screen
                                        System.out.println(JSON.toJSONString(alert, true));

                                        //reset battery low count
                                        batteryLowCount = 0;
                                    }
                                }
                            } else {
                                //timestamp is outside of five minute window
                            }
                        }
                    }
                }
            }
        }

        return alertCount;
    }

    /*
     * If for the same satellite there are three thermostat readings that exceed the red high limit within a five minute interval.
     * @return the number of alerts found
     */
    public int checkForThermostatHighAlert() {

        int thermostatHighCount = 0;
        int alertCount = 0;

        //for each satellite in data
        for (Map.Entry<Integer, Map<Component, List<Telemetry>>> satellite : satelliteTelemetry.entrySet()) {

            //for each component (battery / thermostat)
            for (Map.Entry<Component, List<Telemetry>> component : satellite.getValue().entrySet()) {
                if (component.getKey() == Component.TSTAT) {
                   
                    //loop through telemetry for thermostat component, startindex slides window start
                    List<Telemetry> thermostatList = component.getValue();
                    for (int startIndex = 0; startIndex < thermostatList.size(); startIndex++) {

                        //reset thermostat high count
                        thermostatHighCount = 0;
                       
                        //walk up telemetry list for 5 minute window
                        for (int walkingIndex = startIndex; walkingIndex < thermostatList.size(); walkingIndex++) {

                            //check if timestamp is within 5 min window from start of window
                            if (thermostatList.get(walkingIndex).timestamp < thermostatList.get(startIndex).timestamp + 300000) {
 
                                //Thermostat is greater than red high limit
                                if (thermostatList.get(walkingIndex).rawValue > thermostatList.get(walkingIndex).redHighLimit) {
 
                                    thermostatHighCount++;

                                    if (thermostatHighCount >= 3) {

                                        alertCount++;

                                        Alert alert = new Alert();
                                        alert.setSatelliteId((int) satellite.getKey());
                                        alert.setSeverity("RED HIGH");
                                        alert.setComponent(component.getKey().toString());
                                        alert.setTimestamp(convertJavaTimestampToAlertDate(thermostatList.get(startIndex).timestamp));

                                        //pretty print as json to screen
                                        System.out.println(JSON.toJSONString(alert, true));

                                        //reset thermostat high count
                                        thermostatHighCount = 0;
                                    }
                                }
                            } else {
                                //timestamp is outside of five minute window
                            }
                        }
                    }
                }
            }
        }
        return alertCount;
    }

}
