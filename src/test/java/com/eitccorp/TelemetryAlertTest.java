/*
 * TrimAlgorithmTest.java
 *
 * COPYRIGHT NOTICE
 *
 * (C) 2009 The Johns Hopkins University / Applied Physics Laboratory
 * All Rights Reserved
 *
 * This material may only be used, modified, or reproduced by or for
 * the U.S. Government pursuant to the license rights granted under
 * FAR clause 52.227-14 or DFARS clauses 252.227-7013/7014.
 *
 * For any other permissions, please contact the Legal Office at JHU/APL
 */
package com.eitccorp;


import com.eitccorp.Main;
import java.io.File;
import static org.junit.Assert.assertEquals;
import org.junit.Test;


/**
 * Tests the Telemetry Alerts
 * @author Brian Sherman
 */
public class TelemetryAlertTest
{ 
    @Test
    public void batteryLowAlertTest()
    {   
        Main main = new Main();
        
        File testFile = new File("src/test/java/com/eitccorp/telemetryTest.txt");
        
        main.readTelemetryFile(testFile);
        
        assertEquals( 1, main.checkForBatteryVoltageAlert());
    }

//    @Test
//    public void thermostatHighAlertTest()
//    {   
//        Main main = new Main();
//        main.readTelemetryFile();
//     
//        assertEquals( 1, main.checkForThermostatHighAlert());
//    }
}